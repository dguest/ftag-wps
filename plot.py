#!/usr/bin/env python3

from matplotlib import figure
from pathlib import Path
import yaml, re

def plot_bg(bg):
    fig = figure.Figure()
    ax = fig.subplots()
    here = Path('.')
    for efpath in sorted(here.glob('data/*.yaml')):
        version = efpath.stem.rsplit('_', 1)[-1]
        year = efpath.stem.split('_',1)[0]
        effs = []
        rejs = []
        with open(efpath) as efffile:
            efdict = yaml.safe_load(efffile)['GN2v01']['ttbar']
            for wp, efrej in efdict.items():
                effs.append(efrej['eff']['bjets'])
                rejs.append(efrej['rej'][f'{bg}jets'])
        ls = {'mc20': ':', 'mc23': '-'}[year]
        marker = {
            'final5': 'o',
            'legacy': 'x',
            'newalias': '+',
        }[version]
        color = {
            'final5': 'r',
            'legacy': 'b',
            'newalias': 'g',
        }[version]
        ax.plot(
            effs, rejs,
            label=f'{year}, {version}',
            ls=ls,
            marker=marker,
            color=color
        )
    ax.legend()
    ax.set_xlim(0.5,1)
    ax.set_xlabel(r'$b$-jet efficiency')
    ax.set_yscale('log')
    ax.set_ylim(bottom=1)
    ax.set_ylabel(r'$' f'{bg}' r'$-jet rejection')
    fig.savefig(f'{bg}.pdf')

if __name__ == '__main__':
    plot_bg('u')
    plot_bg('c')
